package;

import Math;
import flixel.FlxG;
import flixel.FlxSprite;
import flixel.math.FlxRect;
import flixel.util.FlxColor;
import flixel.group.FlxSpriteGroup;

class PipePair extends FlxSpriteGroup
{
        private var _sprTop:FlxSprite;
        private var _sprBottom:FlxSprite;
        public var gap:FlxRect;       // gap between top and bottom pipes
        private var _speed:Float = 44;
        private var _margin:Int = 20;
        private var _pipeWidth:Int = 24;
        public var passed:Bool;

        public function new(gapHeight:Int)
        {
                super(FlxG.width, 0, 2); // always start off screen
                gap = new FlxRect();
                gap.set(x, FlxG.random.float(_margin, FlxG.height - _margin - gapHeight), _pipeWidth, gapHeight); 
                _sprBottom = new FlxSprite(0, gap.bottom); 
                _sprTop = new FlxSprite(0, 0);
                passed = false;
                _drawPipes();
                add(_sprBottom);
                add(_sprTop);
                velocity.set(-_speed, 0);
        }

        private function _drawPipes():Void
        {
                _sprBottom.makeGraphic(24, Math.round(FlxG.height - gap.bottom), FlxColor.GREEN);
                _sprTop.makeGraphic(24, Math.round(gap.top),  FlxColor.GREEN);
        }
}
