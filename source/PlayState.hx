package;

import Math;
import flixel.FlxG;
import flixel.FlxSprite;
import flixel.FlxState;
import flixel.util.FlxColor;
import flixel.text.FlxText;
import flixel.ui.FlxButton;
import flixel.math.FlxPoint;
import flixel.group.FlxSpriteGroup;
import flixel.group.FlxGroup.FlxTypedGroup;
import flixel.input.gamepad.FlxGamepad;
import flixel.input.gamepad.FlxGamepadInputID;
import flixel.addons.util.FlxFSM;
import FSMStates;       // Fly, Jump, Fall, Die

class PlayState extends FlxState
{
        public var player:Player;
        public var pipes:Array<PipePair>;
        private var _pipeSep = 80;
        private var _startedPipes = false;
        private var _fsm:FlxFSM<FlxState>;
        public var score:Int;
        private var _scoreTxt:FlxText;

	override public function create():Void
	{
                player = new Player(60, 60);
                add(player);
                pipes = new Array<PipePair>();
                _fsm = new FlxFSM<FlxState>(this);
                _fsm.transitions
                        .add(Fly, Jump, pressedJump)
                        .add(Fall, Jump, pressedJump)
                        .add(Jump, Fall, isAlive)
                        .add(Jump, Die, isColliding)
                        .add(Fall, Die, isColliding)
                        .start(Fly);
                score = 0;
                _scoreTxt = new FlxText(FlxG.width / 2, 30, 0, Std.string(score), 28);
                _scoreTxt.alignment = CENTER;
                _scoreTxt.color = FlxColor.WHITE;
                add(_scoreTxt);
		super.create();
	}

        public function startPipes():Void
        {
                if (!_startedPipes)
                {
                        _addNewPipePair();
                        _startedPipes = true;
                }
        }

	override public function update(elapsed:Float):Void
	{
                _fsm.update(elapsed);
                var lastPipe = pipes[pipes.length - 1];
                if (_startedPipes)
                {
                        if (lastPipe.x + lastPipe.width + _pipeSep < FlxG.width)
                        {
                                remove(_scoreTxt);
                                _addNewPipePair();
                                add(_scoreTxt); // remove and then add score text to keep it in front of everything else (hackish, but works)
                        }
                        var leadPipe = pipes[0];
                        if (leadPipe.x + leadPipe.width < 0)
                        {
                                remove(leadPipe, true);
                                pipes.shift().destroy();
                        }
                        else if (!leadPipe.passed && leadPipe.x + leadPipe.width < player.x)
                        {
                                leadPipe.passed = true;
                                _scoreTxt.text = Std.string(++score);
                        }
                }
		super.update(elapsed);
	}

        private function _addNewPipePair():Void
        {
                var newPipe = new PipePair(60);
                pipes.push(newPipe);
                add(newPipe);
        }
        
        public function pressedJump(owner:FlxState):Bool
        {
                // TODO: touch controls for mobile
                var result:Bool = false;
#if !FLX_NO_KEYBOARD
                result = result || FlxG.keys.justPressed.SPACE;
#end
#if !FLX_NO_GAMEPAD
                var gamepad:FlxGamepad = FlxG.gamepads.lastActive;
                if (gamepad != null)
                {
                        result = result || gamepad.anyJustPressed([
                                        FlxGamepadInputID.A,
                                        FlxGamepadInputID.B,
                                        FlxGamepadInputID.X,
                                        FlxGamepadInputID.Y,
                        ]);
                }
#end
                return result;
        }

        public function isAlive(?owner:FlxState):Bool
        {
                return player.alive;
        }

        // TODO: might want to optimize this (find closest pipes first?)
        public function isColliding(?owner:FlxState):Bool
        {
                var result:Bool = false;
                for (pipePair in pipes)
                {
                        result = result || player.overlaps(pipePair);
                        if (result)
                        {
                                return result;
                        }
                }
                return result || ((player.y + player.height) >= FlxG.height);
        }

}
