package;

import flixel.FlxG;
import flixel.FlxSprite;
import flixel.FlxState;
import flixel.text.FlxText;
import flixel.ui.FlxButton;
import flixel.math.FlxMath;
import flixel.util.FlxAxes;
import flixel.util.FlxColor;

class MenuState extends FlxState
{
        private var _btnPlay:FlxButton;

	override public function create():Void
	{
                _btnPlay = new FlxButton(0, FlxG.height/2, "Play", play);
                _btnPlay.screenCenter(FlxAxes.X);
                add(_btnPlay);
		super.create();
	}

	override public function update(elapsed:Float):Void
	{
		super.update(elapsed);
	}

        private function play():Void
        {
                FlxG.camera.fade(FlxColor.BLACK, .33, false, function()
                {
                        FlxG.switchState(new PlayState());
                });
        }
}
