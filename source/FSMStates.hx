package;

import flixel.FlxG;
import flixel.FlxState;
import flixel.addons.util.FlxFSM;

class Fly extends FlxFSMState<FlxState>
{
        override public function enter(owner:FlxState, fsm:FlxFSM<FlxState>):Void
        {
                FlxG.log.notice('FLY');
                var playState:PlayState = cast (owner, PlayState);
        }
}

class Jump extends FlxFSMState<FlxState>
{

        override public function enter(owner:FlxState, fsm:FlxFSM<FlxState>):Void
        {
                FlxG.log.notice('JUMP');
                var playState:PlayState = cast (owner, PlayState);
                playState.startPipes();
                playState.player.jump();
        }
}

class Fall extends FlxFSMState<FlxState>
{
        override public function enter(owner:FlxState, fsm:FlxFSM<FlxState>):Void
        {
                FlxG.log.notice('FALL');
                var playState:PlayState = cast (owner, PlayState);
                playState.player.tweenDown();
                playState.player.fall();
        }
}

class Die extends FlxFSMState<FlxState>
{
        override public function enter(owner:FlxState, fsm:FlxFSM<FlxState>):Void
        {
                FlxG.log.notice('DIE');
                var playState:PlayState = cast (owner, PlayState);
                // stop pipes
                for (pipePair in playState.pipes)
                {
                        pipePair.velocity.set(0, 0);
                }
                FlxG.camera.shake(0.025, 0.1);
        }
        
        override public function update(elapsed:Float, owner:FlxState, fsm:FlxFSM<FlxState>):Void
        {
                var playState:PlayState = cast (owner, PlayState);
                playState.player.fall();
                if ((playState.player.y + playState.player.height) >= FlxG.height)
                {
                        playState.player.velocity.y = 0;
                        playState.player.acceleration.y = 0;
                        playState.player.kill();
                        FlxG.switchState(new GameOverState(playState.score));
                }
        }
}
