package;

import flixel.FlxG;
import flixel.FlxGame;
import flixel.input.keyboard.FlxKey;
import openfl.Lib;
import openfl.display.Sprite;

class Main extends Sprite
{
	public function new()
	{
		super();
		addChild(new FlxGame(320, 240, MenuState));
#if FLX_KEYBOARD
                // default debugger toggle keys don't work on latin keyboards :(
                FlxG.debugger.toggleKeys.push(COMMA);
#end
	}
}
