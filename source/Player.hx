package;

import flixel.FlxG;
import flixel.FlxSprite;
import flixel.math.FlxPoint;
import flixel.util.FlxColor;
import flixel.tweens.FlxTween;
import flixel.group.FlxSpriteGroup;
import flixel.system.FlxAssets.FlxGraphicAsset;

class Player extends FlxSprite
{
        // gravity should probably be a function of the pipes horizontal _speed or viceversa
        public static inline var GRAVITY:Float = 360;
        public static inline var SPEED:Float = -111;
        private var _jumpSpeed:Float = -111;
        private var _tween:Null<FlxTween>;
        public var minVelocity:FlxPoint;

        public function new(?X:Float=0, ?Y:Float=0)
        {
                minVelocity = new FlxPoint(0, 200);
                super(X, Y);
                maxVelocity.y = 500;
                loadGraphic(AssetPaths.puckman__png, true, 16, 16);
        }

        private function _cancelRunningTweens():Void
        {
                if (_tween != null)
                {
                        _tween.cancel();
                }
        }

        public function jump():Void
        {
                velocity.y = SPEED;
                _cancelRunningTweens();
                // TODO: consider calculating duration of jump (up to peak height) for tween duration
                _tween = FlxTween.angle(this, angle < 0 ? angle : 0 , -50, 0.02);
        }

        public function fall():Void
        {
                acceleration.y = GRAVITY;
        }

        public function tweenDown():Void
        {
                _cancelRunningTweens();
                // TODO: consider calculating how much time to fall to ground for tween duration
                _tween = FlxTween.angle(this, angle, 90, 2);
        }

        override public function update(elapsed:Float):Void
        {
                // keep sprite in screen
                y = y < 0 ? 0 : y;
                y = y + height > FlxG.height ? FlxG.height - height : y;
                super.update(elapsed);
        }
}
