package;

import Math;
import flixel.FlxG;
import flixel.FlxSprite;
import flixel.FlxState;
import flixel.util.FlxColor;
import flixel.util.FlxSave;
import flixel.text.FlxText;
import flixel.ui.FlxButton;
import flixel.util.FlxAxes;
import flixel.group.FlxSpriteGroup;
import flixel.group.FlxGroup.FlxTypedGroup;

class GameOverState extends FlxState
{
        /*      Game Over displays the score and whether it's the highest score.
         *      A button is provided to allow playing again.
         */
        private var _hiScore:Int;
        private var _score:Int;
        private var _saveName:String = 'flixybird-saves';

        public function new(score)
        {
                _score = score;
                var save:FlxSave = new FlxSave();
                save.bind(_saveName);
                _hiScore = save.data.hiScore == null ? 0 : save.data.hiScore;
                if (_score > _hiScore)
                {
                        save.data.hiScore = _score;
                }
                save.close();
                super();
        }

	override public function create():Void
	{
                var btnPlay:FlxButton = new FlxButton(0, FlxG.height/2, "Play Again?", playAgain);
                btnPlay.screenCenter(FlxAxes.X);
                add(btnPlay);
                var scoreTxt:FlxText = new FlxText(FlxG.width / 2 - 60, 30, 0, 'SCORE: ' + Std.string(_score), 28);
                scoreTxt.alignment = CENTER;
                scoreTxt.color = FlxColor.WHITE;
                add(scoreTxt);
                var hiScoreTxt:FlxText = new FlxText(FlxG.width / 2 - 60, 60, 0, 'HI: ' + Std.string(_score > _hiScore ? _score : _hiScore), 28);
                hiScoreTxt.alignment = CENTER;
                hiScoreTxt.color = _score > _hiScore ? FlxColor.YELLOW : FlxColor.WHITE;
                add(hiScoreTxt);
		super.create();
	}

        private function playAgain():Void
        {
                FlxG.camera.fade(FlxColor.BLACK, .33, false, function()
                {
                        FlxG.switchState(new PlayState());
                });
        }

}
